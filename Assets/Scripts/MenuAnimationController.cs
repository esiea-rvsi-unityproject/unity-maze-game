using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Animator))]
public class MenuAnimationController : MonoBehaviour
{
    private const string IS_MENU_OPENED_VARIABLE = "isMenuOpened";

    public bool IsMenuOpened = false;

    private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _animator.updateMode = AnimatorUpdateMode.UnscaledTime;
        SetState();
        Cursor.lockState = CursorLockMode.Confined;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            IsMenuOpened = !IsMenuOpened;
            SetState();
        }
    }

    void SetState()
    {
        _animator.SetBool(IS_MENU_OPENED_VARIABLE, IsMenuOpened);
        Cursor.visible = IsMenuOpened;
        Time.timeScale = (IsMenuOpened) ? 0 : 1;
    }

    public void Continue()
    {
        IsMenuOpened = !IsMenuOpened;
        SetState();
    }

    public void QuitGame()
    {
        SceneManager.LoadScene(0);
    }
}
