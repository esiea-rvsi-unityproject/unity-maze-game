using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUI : MonoBehaviour
{
    [SerializeField]
    private TMPro.TextMeshProUGUI mainKeyText;

    [SerializeField]
    private RawImage sectorKeyImage;

    [SerializeField]
    private Texture redKeyTexture;
    [SerializeField]
    private Texture greenKeyTexture;
    [SerializeField]
    private Texture blueKeyTexture;

    private Inventory playerInventory;

    // Update is called once per frame
    void Update()
    {
        if (playerInventory == null)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            playerInventory = player.GetComponent<Inventory>();
            if (playerInventory == null) return;
            playerInventory.GameItemAdded = OnItemAdded;
            playerInventory.GameItemRemoved = OnItemRemoved;
        }
    }

    void OnItemAdded(GameItemID id, int count)
    {
        switch (id)
        {
            case GameItemID.RedSectorKey:
                ShowSectorImage(redKeyTexture);
                break;
            case GameItemID.GreenSectorKey:
                ShowSectorImage(greenKeyTexture);
                break;
            case GameItemID.BlueSectorKey:
                ShowSectorImage(blueKeyTexture);
                break;
            case GameItemID.MainKey:
                mainKeyText.text = $"{playerInventory.Nb(GameItemID.MainKey)}";
                break;
        }
    }

    void OnItemRemoved(GameItemID id)
    {
        if (id == GameItemID.RedSectorKey || id == GameItemID.BlueSectorKey || id == GameItemID.GreenSectorKey)
            HideSectorImage();
    }

    void ShowSectorImage(Texture tex)
    {
        sectorKeyImage.texture = tex;
        sectorKeyImage.color = Color.white;
    }

    void HideSectorImage()
    {
        sectorKeyImage.texture = null;
        sectorKeyImage.color = Color.clear;
    }
}
