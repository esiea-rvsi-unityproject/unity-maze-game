using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterCameraManipulator : MonoBehaviour
{
    public BetterCamera Camera;

    public Transform Player;

    public Terrain LabyrinthFloor;

    // Start is called before the first frame update
    void Start()
    {
        if (Camera == null) Debug.LogWarning("[BetterCameraManipulator] Camera is missing");        
    }

    // Update is called once per frame
    void Update()
    {
        // if (Input.GetKeyDown(KeyCode.R))
        // {
        //     if (Camera.Locked)
        //     {
        //         Ray ray = new Ray
        //         {
        // 
        //         };
        //         Camera.LockFollowingRay(ray);
        //     }
        // }

        if (Player.transform.hasChanged)
        {
            Player.transform.hasChanged = false;
            Camera.GoToPosition(Player.position);
        }
    }
}
