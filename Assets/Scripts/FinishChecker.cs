using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishChecker : MonoBehaviour
{
    public CanvasGroup uiInformation;

    public float FadingTime = 0.4f;


    private bool uiOpen = false;

    IEnumerator FadeUI(float start, float end)
    {
        float time = 0;

        while (time < FadingTime)
        {
            time += Time.deltaTime;
            uiInformation.alpha = Mathf.Lerp(start, end, time / FadingTime);
            yield return null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.name.Contains("Player")) return;

        Inventory inventory = other.gameObject.GetComponent<Inventory>();
        if (inventory.Nb(GameItemID.MainKey) < 3)
        {
            // game is not finished
            uiOpen = true;
            StartCoroutine(FadeUI(0, 1));
            uiInformation.GetComponent<LookAtPlayer>().enabled = true;
            return;
        }

        // game is finished
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.name.Contains("Player")) return;

        if (uiOpen)
        {
            uiOpen = false;
            StartCoroutine(FadeUI(1, 0));
            uiInformation.GetComponent<LookAtPlayer>().enabled = false;
        }
    }
}
