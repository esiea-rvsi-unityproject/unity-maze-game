using System.Collections.Generic;
using UnityEngine;

public class CollectibleHolder : MonoBehaviour
{
    public float RotationSpeed = 50f;
    public float Range { get => range; set { range = value; PlaceCollectibles(); } }

    [SerializeField]
    private float range = 0.3f;

    public List<Collectible> collectibles;

    private void Start()
    {
        collectibles = new List<Collectible>(GetComponentsInChildren<Collectible>());
        PlaceCollectibles();
    }

    // Update is called once per frame
    void Update()
    {
        transform.localRotation = Quaternion.Euler(Vector3.up * RotationSpeed * Time.realtimeSinceStartup);
    }

    private void OnTransformChildrenChanged()
    {
        collectibles = new List<Collectible>(GetComponentsInChildren<Collectible>());
        PlaceCollectibles();
    }

    void PlaceCollectibles()
    {
        if (collectibles.Count == 0) return;

        if (collectibles.Count == 1)
        {
            Collectible collectible = collectibles[0];
            collectible.transform.localPosition = Vector3.zero;
            return;
        }

        float angleSpace = 2f * Mathf.PI / collectibles.Count;
        for (int i = 0; i < collectibles.Count; i++)
        {
            collectibles[i].transform.localPosition = new Vector3(Mathf.Cos(angleSpace * i), 0f, Mathf.Sin(angleSpace * i)) * Range;
            collectibles[i].transform.LookAt(transform);
        }
    }
}
