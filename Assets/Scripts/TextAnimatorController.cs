using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
[RequireComponent(typeof(Animator))]
public class TextAnimatorController : MonoBehaviour
{
    private Animator _animator;

    private EventTrigger _trigger;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _animator.updateMode = AnimatorUpdateMode.UnscaledTime;
        _trigger = GetComponent<EventTrigger>();

        var onMouseOverEntry = CreateEventTriggerEntry(EventTriggerType.PointerEnter, delegate { Event_OnMouseOver(); });
        var onMouseExitEntry = CreateEventTriggerEntry(EventTriggerType.PointerExit, delegate { Event_OnMouseExit(); });
        var onMouseDownEntry = CreateEventTriggerEntry(EventTriggerType.PointerDown, delegate { Event_OnMouseDown(); });
        var onMouseUpEntry = CreateEventTriggerEntry(EventTriggerType.PointerUp, delegate { Event_OnMouseUp(); });

        _trigger.triggers.Add(onMouseOverEntry);
        _trigger.triggers.Add(onMouseExitEntry);
        _trigger.triggers.Add(onMouseDownEntry);
        _trigger.triggers.Add(onMouseUpEntry);
    }

    EventTrigger.Entry CreateEventTriggerEntry(EventTriggerType eventID, UnityEngine.Events.UnityAction<BaseEventData> callback)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventID;
        entry.callback = new EventTrigger.TriggerEvent();
        entry.callback.AddListener(callback);
        return entry;
    }

    public void Event_OnMouseOver()
    {
        _animator.SetBool("isMouseOver", true);
    }

    public void Event_OnMouseExit()
    {
        _animator.SetBool("isMouseOver", false);
    }
    public void Event_OnMouseDown()
    {
        _animator.SetBool("isMouseDown", true);
    }

    public void Event_OnMouseUp()
    {
        _animator.SetBool("isMouseDown", false);
    }
}
