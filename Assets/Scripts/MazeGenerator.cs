using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MazeGeneration
{

    /*enum WallEnum
    {
        None,
        Top,
        Right,
        Bottom = 4,
        Left = 8
    }*/

    public enum TileDirection
    {
        Top,
        Bottom,
        Left,
        Right,
        NbTileDirections
    }

    public enum TileLinkDirection
    {
        Top = 0,
        Left = 0,
        Bottom = 1,
        Right = 1,
        NbTileLinkDirections = 2
    }

    //
    //    /-----\
    //    |     |
    //    \--|--/
    //       |
    //       |  Vertical
    //       |
    //    /--|--\   Horizontal  /-----\
    //    |     -----------------     |
    //    \-----/               \-----/
    //
    public enum TileLinkOrientation
    {
        Horizontal,
        Vertical,
        NbTileLinkOrientations
    }

    public enum LabyrinthFlagBits
    {
        Visited = 1
    };

    public enum LabyrinthSector
    {
        None,
        Red,
        Blue,
        Green,
        NbSectors
    };

    public enum TileLinkType
    {
        Empty,
        Wall,
        Door,
        NbTileLinkType
    };

    public enum TileContentType
    {
        Empty,
        RedKey,
        BlueKey,
        GreenKey,
        MainKey,
        Tree,
        NbContentTypes
    };

    /// <summary>
    /// Classe mod�le repr�sentant le labyrinthe
    /// Elle stocke le labyrinthe suos forme de tableau et de graphe.
    /// Toutes les cases sont accessibles via le tableau *tiles*. Les cases sont reli�es entree-elle sous forme de graphe symbolisant la liaion entre deux cases.
    /// Cette liaison peut symboliser un mur ou du vide. Elle permet notamment d'introduire d'autre types de parois comme des portes.
    /// Depuis une case, la navigation vers les liens se fait avec l'�num�ration TileDirectionEnum.
    /// Depuis un lien, la vanigation vers les cases se fait avec l'�num�ration TileLinkDirectionEnum.
    /// Les liens sont orient�s � simple but indicatif.
    /// </summary>
    public class Labyrinth
    {
        public class Tile
        {
            public int x, y;    // Coordon�es de la case
            public TileLink[] links;   // Liens vers les autres cases
            public Labyrinth parent;    // Labyrinthe parent
            public byte flags;
            public LabyrinthSector sector;  // Secteur de la case
            public List<TileContentType> content;   // Objets pr�sents sur la case

            public Tile()
            {
                links = new TileLink[(int)TileDirection.NbTileDirections];
                sector = LabyrinthSector.None;
                content = new List<TileContentType>();
            }

            public Tile GetNeighbourTile(TileDirection direction)
            {
                return parent.GetNeighbourTile(this, direction);
            }
        }

        public class TileLink
        {
            public Tile[] tiles;   // Cases r�li�es
            public TileLinkOrientation orientation; // Orientation de la liaison
            public Labyrinth parent;    // Labyrinthe parent
            public byte flags;
            public TileLinkType type;   // Type du lien

            public TileLink()
            {
                type = TileLinkType.Empty;
                tiles = new Tile[(int)TileLinkDirection.NbTileLinkDirections];
            }
        }

        public class DoorLink : TileLink
        {
            public LabyrinthSector sector;

            public DoorLink(TileLink link)
            {
                tiles = link.tiles;
                orientation = link.orientation;
                parent = link.parent;
                flags = link.flags;
                type = TileLinkType.Door;
                sector = LabyrinthSector.None;

                // Relier le nouveau lien aux cases voisines
                RelinkTile(tiles[0], link);
                RelinkTile(tiles[1], link);

                // Remplacer le lien dans la liste du labyrinthe
                int index = parent.links.FindIndex(l => l == link);
                parent.links[index] = this;
            }

            private void RelinkTile(Tile tile, TileLink previousLink)
            {
                if (tile == null) return;

                // Directions de recherche
                TileDirection[] directions = new TileDirection[2];
                switch (orientation)
                {
                    case TileLinkOrientation.Horizontal:
                        directions[0] = TileDirection.Left;
                        directions[1] = TileDirection.Right;
                        break;
                    case TileLinkOrientation.Vertical:
                        directions[0] = TileDirection.Top;
                        directions[1] = TileDirection.Bottom;
                        break;
                }

                for (int i = 0; i < directions.Length; i++)
                    if (tile.links[(int)directions[i]] == previousLink) {
                        tile.links[(int)directions[i]] = this;
                        break;
                    }
            }
        }

        public Tile[,] tiles;  // Cases du labyrinthe
        public List<TileLink> links;   // Liens du labyrinthe
        public uint width;  // Largeur du labyrinthe
        public uint height; // Hauteur du labyrinthe

        public Labyrinth(uint width, uint height)
        {
            this.width = width;
            this.height = height;

            // Cr�er les cases et les liens
            tiles = new Tile[width, height];
            links = new List<TileLink>();

            // Initialiser et relier les cases
            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                {
                    tiles[x, y] = new Tile();
                    tiles[x, y].parent = this;
                    InitTile(ref tiles[x, y], x, y);
                }
        }

        private void InitTile(ref Tile tile, int x, int y)
        {
            tile.x = x;
            tile.y = y;

            for (int i = 0; i < (int)TileDirection.NbTileDirections; i++)
                if (tile.links[i] == null)
                {    // v�rifier si aucun lien n'existe d�j�

                    TileLink link = FindTileLinkFromNeighbour(tile, (TileDirection)i);
                    if (link != null)   // si le voisin dans la direction souhait�e poss�de un lien, s'attacher au lien
                    {
                        tile.links[i] = GetNeighbourTile(tile, (TileDirection)i).links[(int)GetOppositionTileDirection((TileDirection)i)];
                    }
                    else // sinon en cr�er un nouveau
                    {
                        tile.links[i] = new TileLink();
                        tile.links[i].parent = this;

                        // modification de l'orientation du lien
                        if (i == (int)TileDirection.Top || i == (int)TileDirection.Bottom)
                            tile.links[i].orientation = TileLinkOrientation.Vertical;
                        else
                            tile.links[i].orientation = TileLinkOrientation.Horizontal;

                        links.Add(tile.links[i]);
                    }
                }

            // relier le lien � la case actuelle
            tile.links[(int)TileDirection.Top].tiles[(int)TileLinkDirection.Bottom] = tile;
            tile.links[(int)TileDirection.Bottom].tiles[(int)TileLinkDirection.Top] = tile;
            tile.links[(int)TileDirection.Left].tiles[(int)TileLinkDirection.Right] = tile;
            tile.links[(int)TileDirection.Right].tiles[(int)TileLinkDirection.Left] = tile;
        }

        /// <summary>
        /// Trouve le lien entre deux cases en passant par le voisin
        /// </summary>
        /// <param name="tile">Case de d�part</param>
        /// <param name="direction">Direction du lien � trouver</param>
        /// <returns>Lien dans la direction donn�e</returns>
        private TileLink FindTileLinkFromNeighbour(Tile tile, TileDirection direction)
        {
            Tile neighbourTile = GetNeighbourTile(tile, direction);
            TileDirection oppositeDirection = GetOppositionTileDirection(direction);
            return neighbourTile?.links[(int)oppositeDirection] ?? null;
        }

        /// <summary>
        /// R�cup�re la case voisine dans la direction pr�cis�e
        /// </summary>
        /// <param name="tile">Case de d�part</param>
        /// <param name="direction">Direction souhait�e</param>
        /// <returns>Case voisine</returns>
        public Tile GetNeighbourTile(Tile tile, TileDirection direction)
        {
            int offsetX, offsetY;
            GetDirectionOffset(direction, out offsetX, out offsetY);
            int neighbourX = tile.x + offsetX, neighbourY = tile.y + offsetY;
            if (IsCoordInRange(neighbourX, neighbourY))
                return tiles[neighbourX, neighbourY];
            return null;
        }

        /// <summary>
        /// Donne le d�calage � faire correspondant � la direction donn�e
        /// </summary>
        /// <param name="direction">Direction du d�calage</param>
        /// <param name="x">D�calage en X � r�aliser</param>
        /// <param name="y">D�calage en Y � r�aliser</param>
        public static void GetDirectionOffset(TileDirection direction, out int x, out int y)
        {
            switch (direction)
            {
                case TileDirection.Top:
                    x = 0;
                    y = -1;
                    break;
                case TileDirection.Bottom:
                    x = 0;
                    y = 1;
                    break;
                case TileDirection.Left:
                    x = -1;
                    y = 0;
                    break;
                case TileDirection.Right:
                    x = 1;
                    y = 0;
                    break;
                default:
                    x = 0;
                    y = 0;
                    break;
            }
        }

        /// <summary>
        /// Donne la direction oppos�e
        /// </summary>
        /// <param name="direction">Direction initiale</param>
        /// <returns>Direction oppos�e</returns>
        public static TileDirection GetOppositionTileDirection(TileDirection direction)
        {
            if (direction >= TileDirection.NbTileDirections)
                return direction;
            return direction + (((int)direction % 2 == 0) ? 1 : -1);
        }

        /// <summary>
        /// V�rifie si les coordon�es donn�es sont dans le labyrinthe
        /// </summary>
        /// <param name="x">Coordonn�e X</param>
        /// <param name="y">Coordonn�e T</param>
        /// <returns>true si les coordon�es sont contenues dans le labyrinthe, false sinon</returns>
        public bool IsCoordInRange(int x, int y)
        {
            return x >= 0 && x < width && y >= 0 && y < height;
        }

        /// <summary>
        /// Donne les cases accessibles depuis une case pr�cise
        /// </summary>
        /// <returns>Liste des cases accessibles</returns>
        public List<Tile> AccessibleTilesFrom(Tile tile, byte visited_bit)
        {
            List<Tile> tiles = new List<Tile>();
            Stack<Tile> visited = new Stack<Tile>();

            visited.Push(tile);
            while (visited.Count > 0)
            {
                Tile curTile = visited.Pop();
                tiles.Add(curTile);
                curTile.flags |= visited_bit;

                for (int i = 0; i < (int)TileDirection.NbTileDirections; i++)
                {
                    Tile nextTile = GetNeighbourTile(curTile, (TileDirection)i);
                    if (nextTile != null && curTile.links[i].type == TileLinkType.Empty && (nextTile.flags & visited_bit) == 0)
                        visited.Push(nextTile);
                }
            }

            return tiles;
        }

        /// <summary>
        /// R�initialise le bit sp�cifi� � 0
        /// </summary>
        /// <param name="flag_bit"></param>
        public void ResetFlagBit(byte flag_bit)
        {
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    if ((tiles[i, j].flags & flag_bit) != 0)
                        tiles[i, j].flags -= flag_bit;
        }

        /// <summary>
        /// Liste toutes les cases qui sont des impasses
        /// </summary>
        /// <param name="blocking_bit"></param>
        /// <returns></returns>
        public List<Tile> GetEndpoints()
        {
            List<Tile> endpoints = new List<Tile>();

            foreach (var tile in tiles)
            {
                int nbWalls = 0;
                for (int i = 0; i < (int)TileDirection.NbTileDirections; i++)
                    if (tile.links[i].type != TileLinkType.Empty)
                        nbWalls++;
                if (nbWalls == 3)
                    endpoints.Add(tile);
            }

            return endpoints;
        }

        /// <summary>
        /// Trouve le chemin le plus court entre deux cases
        /// </summary>
        /// <param name="start"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public List<Tile> Path(Tile start, Tile to, byte visited_bit)
        {
            start.flags |= visited_bit;

            if (start == to)
            {
                List<Tile> res = new List<Tile>();
                res.Add(to);
                return res;
            }

            List<Tile> neighbours = new List<Tile>();
            for (int i = 0; i < (int)TileDirection.NbTileDirections; i++)
            {
                Tile neighbour = GetNeighbourTile(start, (TileDirection)i);
                if (neighbour != null && (neighbour.flags & visited_bit) == 0 && start.links[i].type == TileLinkType.Empty)
                    neighbours.Add(neighbour);
            }

            if (neighbours.Count == 0) return null;

            System.Func<Tile, Tile, int> eval = (tile1, tile2) => (tile2.x - tile1.x + tile2.y - tile1.y);

            while (neighbours.Count > 0)
            {
                int nextIndex = 0;
                int nextScore = eval(neighbours[0], to);
                // find nearest neighbour
                for (int i = 1; i < neighbours.Count; i++)
                {
                    int score = eval(neighbours[i], to);
                    if (score > nextScore)
                    {
                        nextScore = score;
                        nextIndex = i;
                    }
                }

                List<Tile> neighbourPath = Path(neighbours[nextIndex], to, visited_bit);
                if (neighbourPath != null)
                {
                    neighbourPath.Insert(0, start);
                    return neighbourPath;
                }

                neighbours.RemoveAt(nextIndex);
            }

            return null;
        }
    }

    public class MazeGenerator
    {
        public static System.Action<System.Exception> ExceptionNotify = null;

        public class RecursiveBackTrackerProcedure
        {
            Labyrinth labyrinth;
            Vector2Int from;

            public RecursiveBackTrackerProcedure(ref Labyrinth labyrinth)
            {
                this.labyrinth = labyrinth;
                from = Vector2Int.zero;
            }

            public void SetStartingPoint(Vector2Int from)
            {
                if (labyrinth == null)
                {
                    this.from = from;
                    return;
                }

                if (labyrinth.IsCoordInRange(from.x, from.y))
                    this.from = from;
            }

            public void Apply()
            {
                foreach (var link in labyrinth.links)
                    link.type = TileLinkType.Wall;

                RecursiveProcedure(labyrinth.tiles[from.x, from.y]);
            }

            private void RecursiveProcedure(Labyrinth.Tile tile)
            {
                tile.flags |= (int)LabyrinthFlagBits.Visited;

                List<TileDirection> possibleNeighbours = new List<TileDirection>();
                for (int i = 0; i < (int)TileDirection.NbTileDirections; i++)
                    if (tile.links[i].type == TileLinkType.Wall)
                        possibleNeighbours.Add((TileDirection)i);

                int randDirection;
                TileDirection direction;
                for (int i = 0; i < possibleNeighbours.Count; i++)
                {
                    randDirection = UnityEngine.Random.Range(0, possibleNeighbours.Count);
                    direction = possibleNeighbours[randDirection];

                    Labyrinth.Tile nextTile = tile.GetNeighbourTile(direction);
                    if (nextTile != null && (nextTile.flags & (int)LabyrinthFlagBits.Visited) == 0)
                    {
                        tile.links[(int)direction].type = TileLinkType.Empty;
                        RecursiveProcedure(nextTile);
                    }
                    possibleNeighbours.RemoveAt(randDirection);
                    i--;
                }
            }
        }

        /// <summary>
        /// G�n�re le labyrinthe
        /// La fonction ExceptionCallback de la classe est appel� quand une exception est lev�e. Si une exception est lev�e, le temps de g�n�ration risque d'augmenter.
        /// </summary>
        /// <param name="width">Largeur</param>
        /// <param name="height">Hauteur</param>
        /// <param name="from">Point de d�part dans le labyrinthe</param>
        /// <param name="algo">Algorithme de g�n�ration � utiliser</param>
        /// <returns></returns>
        public static Labyrinth GenerateLabyrinth(int width, int height, Vector2Int from, AlgoEnum algo = AlgoEnum.Rbacktracker)
        {
            Labyrinth labyrinth = null;
            do
            {
                try
                {
                    labyrinth = GenerateLabyrinthWalls(width, height, from, algo);
                    GenerateDoorsAndCollectibles(ref labyrinth, from);
                } catch (LabyrinthNotUsable exception) {
                    labyrinth = null;
                    // Appel du callback pour informer l'utilisateur
                    ExceptionNotify?.Invoke(exception);
                }
            } while (labyrinth == null);    // Tant que le labyrinthe n'est pas bon, on en g�n�re un nouveau

            return labyrinth;
        }

        /// <summary>
        /// G�n�re les murs du labyrinthe
        /// </summary>
        /// <param name="width">Largeur</param>
        /// <param name="height">Hauteur</param>
        /// <param name="from">Point de d�part dans le labyrinthe</param>
        /// <param name="algo">Algorithme de g�n�ration � utiliser</param>
        /// <returns>Labyrinthe avec les murs</returns>
        private static Labyrinth GenerateLabyrinthWalls(int width, int height, Vector2Int from, AlgoEnum algo = AlgoEnum.Rbacktracker)
        {
            Labyrinth labyrinth = new Labyrinth((uint)width, (uint)height);

            RecursiveBackTrackerProcedure procedure = new RecursiveBackTrackerProcedure(ref labyrinth);
            procedure.SetStartingPoint(from);
            procedure.Apply();

            ResetVisitedFlagOnAllTiles(ref labyrinth);

            return labyrinth;
        }

        /// <summary>
        /// Positionne des arbres dans les impasses libres
        /// </summary>
        /// <param name="labyrinth"></param>
        /// <param name="nbTrees"></param>
        public static void GenerateTrees(ref Labyrinth labyrinth, int nbTrees)
        {
            List<Labyrinth.Tile> endpoints = labyrinth.GetEndpoints();
            endpoints.RemoveAll(endpoint => endpoint.content.Count > 0);

            if (nbTrees > endpoints.Count)
                nbTrees = endpoints.Count;

            for (int i = 0; i < nbTrees; i++)
            {
                int endpointIndex = Random.Range(0, endpoints.Count);
                endpoints[endpointIndex].content.Add(TileContentType.Tree);
                endpoints.RemoveAt(endpointIndex);
            }
        }

        /// <summary>
        /// Retire le mur qui sert de bordure le plus proche
        /// </summary>
        /// <param name="labyrinth"></param>
        /// <param name="from"></param>
        public static void RemoveBorderWallNextToPosition(ref Labyrinth labyrinth, Vector2Int from)
        {
            TileDirection direction = TileDirection.NbTileDirections;
            if (from.x == 0)
                direction = TileDirection.Left;
            else if (from.x == labyrinth.width - 1)
                direction = TileDirection.Right;
            else if (from.y == 0)
                direction = TileDirection.Top;
            else if (from.y == labyrinth.height - 1)
                direction = TileDirection.Bottom;

            labyrinth.tiles[from.x, from.y].links[(int)direction].type = TileLinkType.Empty;
        }

        /// <summary>
        /// G�n�re un ordre de secteurs al�atoire
        /// </summary>
        /// <returns></returns>
        private static List<LabyrinthSector> GenerateSectorOrder()
        {
            List<LabyrinthSector> sectors = new List<LabyrinthSector>();
            int nbSectors = (int)LabyrinthSector.NbSectors - (int)LabyrinthSector.Red;
            for (int i = 0; i < nbSectors; i++)
            {
                LabyrinthSector sector;
                do
                {   // Cherche un secteur al�atoire qui n'est pas d�j� utilis�
                    sector = (LabyrinthSector)Random.Range((int)LabyrinthSector.Red, (int)LabyrinthSector.NbSectors);
                } while (sectors.Contains(sector));
                sectors.Add(sector);
            }
            return sectors;
        }

        /// <summary>
        /// Supprime les enfants et sous-enfants du noeud parent pr�sents dans la liste fournie
        /// </summary>
        /// <param name="nodes">Liste de noeuds</param>
        /// <param name="parent">Noeud parent</param>
        private static void RemoveParentsAndChildrenInList(ref List<LabyrinthTree.Node> nodes, LabyrinthTree.Node parent)
        {
            for (int j = 0; j < nodes.Count; j++)
            {
                LabyrinthTree.Node node = nodes[j];
                if (node.IsChildOf(parent) || node == parent || parent.IsChildOf(node))
                    nodes.RemoveAt(j--);
            }
        }

        /// <summary>
        /// Trouve un noeud pour placer une porte
        /// </summary>
        /// <param name="remainingIntersections">Liste des intersections possibles</param>
        /// <param name="minNbEndpoints">Minimum d'impasses avant la porte</param>
        /// <returns></returns>
        private static LabyrinthTree.Node FindDoorNode(List<LabyrinthTree.Node> remainingIntersections, int minNbEndpoints)
        {
            LabyrinthTree.Node doorNode, blockedNode;
            List<LabyrinthTree.Node> possibleBlockedNodes;
            int tries = 0;
            do
            {
                doorNode = remainingIntersections[Random.Range(0, remainingIntersections.Count)];   // Choisir une intersection al�atoire
                possibleBlockedNodes = new List<LabyrinthTree.Node>();
                for (int j = 0; j < doorNode.nextTiles.Count; j++)
                    if (doorNode.nextTiles[j].nbEndpoints >= minNbEndpoints)    // R�cup�rer les enfants du noeud ayant le minimum d'impasses requis
                        possibleBlockedNodes.Add(doorNode.nextTiles[j]);

                // R�p�ter tant que aucun enfant du noeud choisi satisfait la condition
            } while (possibleBlockedNodes.Count == 0 && ++tries < remainingIntersections.Count); 

            if (tries == remainingIntersections.Count)  // Si aucune intersection n'a �t� trouv�, prendre la premi�re disponible
                blockedNode = remainingIntersections[Random.Range(0, remainingIntersections.Count)].nextTiles[0];
            else
                blockedNode = possibleBlockedNodes[Random.Range(0, possibleBlockedNodes.Count)];

            return blockedNode;
        }

        /// <summary>
        /// G�n�re la position de chaque porte pour chaque secteur
        /// </summary>
        /// <param name="tree">Arbre du labyrinthe</param>
        /// <param name="sectorOrder">Ordre des secteurs</param>
        /// <param name="minNbEndpoints">Minimum d'impasses avant une porte</param>
        /// <returns></returns>
        private static List<LabyrinthTree.Node> GenerateDoorPositions(ref LabyrinthTree tree, List<LabyrinthSector> sectorOrder, int minNbEndpoints)
        {
            const int MAX_NB_TRIES = 10;

            // R�cup�re les intersections avec le minimum d'impasses et au maximum 2 fois le nombre d'impasses minimum
            // Cela �vite d'avoir des intersections qui bouche une trop grosse partie du labyrinthe
            List<LabyrinthTree.Node> intersections = new List<LabyrinthTree.Node>(tree.intersections);
            intersections.RemoveAll(node => (node.nbEndpoints < minNbEndpoints) || (node.nbEndpoints >= 2 * minNbEndpoints));

            List<LabyrinthTree.Node> doors = new List<LabyrinthTree.Node>();
            int globalTries = 0, orderIndex = 0;
            do
            {
                // R�initialise la liste des portes
                doors.Clear();

                // Liste des intersections encore disponibles
                List<LabyrinthTree.Node> tmpIntersections = new List<LabyrinthTree.Node>(intersections);
                for (orderIndex = 0; orderIndex < sectorOrder.Count; orderIndex++)
                {
                    // Si aucune intersection ne reste, la g�n�ration de porte est invalide
                    if (tmpIntersections.Count == 0)
                        break;
                    
                    // Trouve le noeud � boucher (intersection juste apr�s la porte)
                    LabyrinthTree.Node blockedNode = FindDoorNode(tmpIntersections, minNbEndpoints);

                    doors.Add(blockedNode);

                    // Supprime les intersections qui ne sont plus accessible et les intersections qui peuvent boucher l'intersection de la porte
                    RemoveParentsAndChildrenInList(ref tmpIntersections, blockedNode);
                }

                // R�p�ter tant qu'il n'a pas �t� possible de positionner une porte par secteur
            } while (orderIndex < sectorOrder.Count && ++globalTries < MAX_NB_TRIES);

            // Si toutes les tentatives ont �chou�, le labyrinthe n'est plus utilisable
            if (orderIndex < sectorOrder.Count)
                throw new LabyrinthNotUsable();

            return doors;
        }

        /// <summary>
        /// Marque les cases apr�s la porte avec le secteur associ�
        /// </summary>
        /// <param name="doorNode">Noeud contenant la porte</param>
        /// <param name="linkLimitIndex">Index du lien marqu� comme porte</param>
        /// <param name="sector">Secteur associ�</param>
        private static void MarkTileSector(LabyrinthTree.Node doorNode, int linkLimitIndex, LabyrinthSector sector)
        {
            int i;
            Stack<LabyrinthTree.Node> nodesToMark = new Stack<LabyrinthTree.Node>();
            for (i = 0; i < doorNode.nextTiles.Count; i++)
                nodesToMark.Push(doorNode.nextTiles[i]);

            while (nodesToMark.Count > 0)
            {
                LabyrinthTree.Node curNode = nodesToMark.Pop();

                for (i = 0; i < curNode.pathTiles.Count; i++)
                    curNode.pathTiles[i].sector = sector;

                for (i = 0; i < curNode.nextTiles.Count; i++)
                    nodesToMark.Push(curNode.nextTiles[i]);
            }

            for (i = linkLimitIndex; i < doorNode.pathTiles.Count; i++)
                doorNode.pathTiles[i].sector = sector;
        }

        /// <summary>
        /// Marque un lien pr�c�dent les intersections des portes
        /// </summary>
        /// <param name="doors">Intersections des portes</param>
        private static void MarkDoorLinks(ref List<LabyrinthTree.Node> doors, List<LabyrinthSector> order)
        {
            if (doors.Count != order.Count) Debug.LogWarning("The number of doors generated is different of the number of sectors");
            for (int i = 0; i < doors.Count; i++) {
                int index = Random.Range(0, doors[i].pathLinks.Count);
                Labyrinth.TileLink link = doors[i].pathLinks[index];
                Labyrinth.DoorLink doorLink = new Labyrinth.DoorLink(link);
                doorLink.sector = order[i];
                doors[i].pathLinks[index] = doorLink;
                MarkTileSector(doors[i], index, order[i]);
            }
        }

        /// <summary>
        /// Donne le cl� associ�e � la couleur du secteur donn�
        /// </summary>
        /// <param name="sector"></param>
        /// <returns></returns>
        private static TileContentType GetContentKeyFromSectorId(LabyrinthSector sector)
        {
            switch (sector)
            {
                case LabyrinthSector.Red:
                    return TileContentType.RedKey;
                case LabyrinthSector.Blue:
                    return TileContentType.BlueKey;
                case LabyrinthSector.Green:
                    return TileContentType.GreenKey;
                default:
                    return TileContentType.Empty;
            }
        }

        /// <summary>
        /// G�n�re la position des cl�s et marque les impasses
        /// </summary>
        /// <param name="tree">Arbre du labyrinthe</param>
        /// <param name="doors">Intersections contenant des portes</param>
        private static void GenerateAndMarkKeyPositions(ref LabyrinthTree tree, List<LabyrinthTree.Node> doors, List<LabyrinthSector> order)
        {
            // Liste des impasses restantes
            List<LabyrinthTree.Node> remainingEndpoints = new List<LabyrinthTree.Node>(tree.endpoints);
            // Cette liste permet � la fin de retrouver les impasses qui ne sont pas dans un secteur du labyrinthe

            for (int i = doors.Count - 1; i >= 0; i--)
            {
                // Liste des impasses du secteur
                List<LabyrinthTree.Node> sectorEndpoints = new List<LabyrinthTree.Node>();
                for (int j = 0; j < remainingEndpoints.Count; j++)
                {
                    // Si l'impasse est un enfant de l'intersection bouch�e par une porte
                    if (remainingEndpoints[j].IsChildOf(doors[i]) || doors[i] == remainingEndpoints[j])
                    {
                        sectorEndpoints.Add(remainingEndpoints[j]); // Ajouter l'impasse dans les impasses du secteur
                        remainingEndpoints.RemoveAt(j--);
                    }
                }

                // Marque deux impasses du secteur avec une cl� pour le secteur suivant et une cl� pour la porte de fin
                if (i != doors.Count - 1)
                {
                    TileContentType key = GetContentKeyFromSectorId(order[i + 1]);
                    sectorEndpoints[Random.Range(0, sectorEndpoints.Count)].tile.content.Add(key);
                }
                sectorEndpoints[Random.Range(0, sectorEndpoints.Count)].tile.content.Add(TileContentType.MainKey);
            }

            // Ajoute la cl� du premier secteur dans les impasses non bouch�es
            // S'il ne reste aucune impasse, placer la cl� dans la premi�re intersection
            TileContentType firstKey = GetContentKeyFromSectorId(order[0]);
            if (remainingEndpoints.Count == 0)
                tree.root.nextTiles[0].tile.content.Add(firstKey);
            else
                remainingEndpoints[Random.Range(0, remainingEndpoints.Count)].tile.content.Add(firstKey);
        }

        /// <summary>
        /// G�n�re les portes et les cl�s dans le labyrinthe
        /// </summary>
        /// <param name="labyrinth">Labyrinthe</param>
        /// <param name="from">Position de d�part du labyrinthe</param>
        public static void GenerateDoorsAndCollectibles(ref Labyrinth labyrinth, Vector2Int from)
        {
            LabyrinthTree tree = new LabyrinthTree(ref labyrinth);
            tree.Build(labyrinth.tiles[from.x, from.y]);

            // G�n�re l'ordre des secteurs
            List<LabyrinthSector> sectorOrder = GenerateSectorOrder();

            // S'il n'y a pas assez d'intersections, le labyrinthe est invalide
            if (sectorOrder.Count > tree.intersections.Count)
                throw new LabyrinthNotUsable();

            // Nombre minimum d'impasses avant de placer une porte
            int minNbEndpoints = tree.root.nbEndpoints / 8;

            // Placer et les portes et les cl�s dans le labyrinthe
            List<LabyrinthTree.Node> doors = GenerateDoorPositions(ref tree, sectorOrder, minNbEndpoints);

            MarkDoorLinks(ref doors, sectorOrder);

            GenerateAndMarkKeyPositions(ref tree, doors, sectorOrder);
        }

        /// <summary>
        /// Trouve les deux impasses les plus �loign�es
        /// </summary>
        /// <param name="labyrinth">Labyrinthe</param>
        /// <returns></returns>
        private static Labyrinth.Tile[] FindFarthestEndpoints(ref Labyrinth labyrinth)
        {
            List<Labyrinth.Tile> endpoints = labyrinth.GetEndpoints();

            Labyrinth.Tile[] farthestEndpoints = new Labyrinth.Tile[2];
            int biggestDistance = 0;
            for (int i = 0; i < endpoints.Count; i++)
            {
                for (int j = i + 1; j < endpoints.Count; j++)
                {
                    List<Labyrinth.Tile> path = labyrinth.Path(endpoints[i], endpoints[j], (int)LabyrinthFlagBits.Visited);
                    if (path.Count > biggestDistance)
                    {
                        farthestEndpoints[0] = endpoints[i];
                        farthestEndpoints[1] = endpoints[j];
                    }

                    ResetVisitedFlagOnAllTiles(ref labyrinth);
                }
            }

            return farthestEndpoints;
        }

        /// <summary>
        /// Retire le flag "visit�" des cases du labyrinthe
        /// </summary>
        /// <param name="labyrinth">Labyrinthe</param>
        public static void ResetVisitedFlagOnAllTiles(ref Labyrinth labyrinth)
        {
            labyrinth.ResetFlagBit((int)LabyrinthFlagBits.Visited);
        }

        /// <summary>
        /// Cette classe repr�sente l'arbre d'un labyrinthe.
        /// Le noeud racine repr�sente le point de d�part. Tous les enfants et sous-enfants de ce noeud repr�sente soit une intersection, soit une impasse.
        /// C'est une intersection si le noeud poss�de plusieurs enfants, une impasse s'il poss�de aucun enfant.
        /// Cela permet de distinguer les impasses les plus �loign�es du point de d�part et voir les intersections � prendre pour atteindre une impasse.
        /// </summary>
        public class LabyrinthTree
        {
            /// <summary>
            /// Noeud de l'arbre de labyrinthe
            /// </summary>
            public class Node
            {
                public Labyrinth.Tile tile; // Case du noeud
                public List<Node> nextTiles;    // Noeuds suivants
                public int distance;       // Nombre de cases parcourues depuis le point de d�part
                public Node previousTile;   // Parent du noeud
                public List<Labyrinth.Tile> pathTiles;  // Cases parcourues depuis le parent
                public List<Labyrinth.TileLink> pathLinks;  // Liens parcourues depuis le parent
                public int nbEndpoints; // Nombre d'impasses sous le noeud


                public Node()
                {
                    nextTiles = new List<Node>();
                    pathTiles = new List<Labyrinth.Tile>();
                    pathLinks = new List<Labyrinth.TileLink>();
                    previousTile = null;
                    distance = 0;
                    nbEndpoints = 0;
                }

                /// <summary>
                /// Indique si le noeud est un fils du noeud sp�cifi�
                /// </summary>
                /// <param name="node">Parent</param>
                /// <returns>true si le noeud est parent</returns>
                public bool IsChildOf(Node node)
                {
                    if (node == this) return false;

                    Node curNode = this;
                    while (curNode.previousTile != null)
                    {
                        curNode = curNode.previousTile;
                        if (curNode == node) return true;
                    }

                    return false;
                }

                /// <summary>
                /// R�cup�re les noeuds � une certaine profondeur
                /// </summary>
                /// <param name="depth">Profondeur de recherche</param>
                /// <returns></returns>
                public List<Node> ChildrenInDepth(int depth)
                {
                    if (depth == 0) return new List<Node> { this };
                    List<Node> children = new List<Node>();
                    for (int i = 0; i < nextTiles.Count; i++)
                        children.AddRange(nextTiles[i].ChildrenInDepth(depth - 1));
                    return children;
                }
            }

            public Node root;   // Racine de l'arbre
            public List<Node> endpoints;    // Liste des impasses
            public List<Node> intersections;    // Liste des intersections
            public Labyrinth labyrinth; // Labyrinthe associ� de l'arbre

            public LabyrinthTree(ref Labyrinth labyrinth)
            {
                root = null;
                endpoints = new List<Node>();
                intersections = new List<Node>();
                this.labyrinth = labyrinth;
            }

            /// <summary>
            /// Contruit l'arbre � partir du labyrinthe fourni dans le constructeur
            /// </summary>
            /// <param name="from">Case de d�part de l'arbre</param>
            public void Build(Labyrinth.Tile from)
            {
                ResetVisitedFlagOnAllTiles(ref labyrinth);

                root = new Node
                {
                    tile = from
                };

                // Premier noeud de parcours
                Node firstNode = new Node
                {
                    tile = from,
                    previousTile = root
                };
                root.nextTiles.Add(firstNode);

                // Pile des noeuds � parcourir
                Stack<Node> nodeStack = new Stack<Node>();
                nodeStack.Push(firstNode);

                while (nodeStack.Count > 0)
                {
                    Node curNode = nodeStack.Pop();
                    // Marque la case comme parcourue
                    curNode.tile.flags |= (int)LabyrinthFlagBits.Visited;

                    // Cherche les cases non parcourues en suivant le chemin (on ne traverse pas les murs)
                    List<(Labyrinth.Tile, Labyrinth.TileLink)> nextTiles = new List<(Labyrinth.Tile, Labyrinth.TileLink)>();
                    for (int i = 0; i < (int)TileDirection.NbTileDirections; i++)
                    {
                        Labyrinth.Tile nextTile = curNode.tile.GetNeighbourTile((TileDirection)i);
                        if (nextTile != null && (nextTile.flags & (int)LabyrinthFlagBits.Visited) == 0 && curNode.tile.links[i].type == TileLinkType.Empty)
                            nextTiles.Add((nextTile, curNode.tile.links[i]));
                    }

                    switch (nextTiles.Count)
                    {
                        case 0: // Si aucune case n'est accessible, on est dans une impasse
                            endpoints.Add(curNode);
                            break;
                        case 1:     // Si une seule case est accessible, on est dans un chemin.
                            // On suit le chemin
                            curNode.tile = nextTiles[0].Item1;

                            // Ajoute la case parcourue et le lien utilis� dans le parcours du noeud
                            curNode.pathTiles.Add(nextTiles[0].Item1);
                            curNode.pathLinks.Add(nextTiles[0].Item2);

                            curNode.distance++;
                            nodeStack.Push(curNode);
                            break;

                        default:    // S'il y a plusieurs cases accessibles, c'est une intersection
                            intersections.Add(curNode);

                            // On ne fait plus parcourir le noeud courant (pour qu'il reste � l'intersection)
                            // et on cr�e un noeud pour chaque case accessible
                            for (int i = 0; i < nextTiles.Count; i++)
                            {
                                Node nextNode = new Node
                                {
                                    tile = nextTiles[i].Item1,
                                    previousTile = curNode,
                                    distance = curNode.distance + 1,
                                    pathTiles = new List<Labyrinth.Tile> { nextTiles[i].Item1 },
                                    pathLinks = new List<Labyrinth.TileLink> { nextTiles[i].Item2 }
                                };
                                nodeStack.Push(nextNode);
                                curNode.nextTiles.Add(nextNode);
                            }
                            break;
                    }
                }

                // Pour chaque impasse, on incr�mente le nombre d'impasses de ses parents
                foreach (var endpoint in endpoints)
                {
                    Node curNode = endpoint;
                    while (curNode != null)
                    {
                        curNode.nbEndpoints++;
                        curNode = curNode.previousTile;
                    }
                }

                ResetVisitedFlagOnAllTiles(ref labyrinth);
            }
        }

        /// <summary>
        /// Cette exception est lev�e quand toutes les tentatives de g�n�ration ont �chou�s
        /// </summary>
        private class LabyrinthNotUsable : System.Exception
        {
        }

        /*
        public static void GenLabyrinth(ref tableau[,] laby, Vector2 start, AlgoEnum algo, int width, int height, ref Vector2 fin)
        {
            laby[(char)start.x, (char)start.y].visited = true;
            byte border = CheckBorder(start, width, height);
            byte visit = Visited(ref laby, start, width, height, border);
            if (visit == 0)
            {
                fin = start;
                return;
            }
            List<byte> voisinDispo = new List<byte>();
            {
                if ((visit & 1) != 0)
                {
                    voisinDispo.Add(1);
                }
                if ((visit & 2) != 0)
                {
                    voisinDispo.Add(2);
                }
                if ((visit & 4) != 0)
                {
                    voisinDispo.Add(4);
                }
                if ((visit & 8) != 0)
                {
                    voisinDispo.Add(8);
                }
            }
            int randCase; 
            int nbVoision = voisinDispo.Count;
            for (int i = 0; i < nbVoision; i++)
            {
                randCase = Random.Range(0, voisinDispo.Count);
                switch (voisinDispo[randCase])
                {
                    case 1:
                        if (laby[(char)start.x - 1, (char)start.y].visited == false)
                        {
                            laby[(char)start.x, (char)start.y].value -= (char)WallEnum.Top;
                            laby[(char)start.x - 1, (char)start.y].value -= (char)WallEnum.Bottom;
                            GenLabyrinth(ref laby, new Vector2(start.x - 1, start.y), algo, width, height, ref fin);
                        }
                        break;
                    case 2:
                        if (laby[(char)start.x, (char)start.y + 1].visited == false)
                        {
                            laby[(char)start.x, (char)start.y].value -= (char)WallEnum.Right;
                            laby[(char)start.x, (char)start.y + 1].value -= (char)WallEnum.Left;
                            GenLabyrinth(ref laby, new Vector2(start.x, start.y + 1), algo, width, height, ref fin);
                        }
                        break;
                    case 4:
                        if (laby[(char)start.x + 1, (char)start.y].visited == false)
                        {
                            laby[(char)start.x, (char)start.y].value -= (char)WallEnum.Bottom;
                            laby[(char)start.x + 1, (char)start.y].value -= (char)WallEnum.Top;
                            GenLabyrinth(ref laby, new Vector2(start.x + 1, start.y), algo, width, height, ref fin);
                        }
                        break;
                    case 8:
                        if (laby[(char)start.x, (char)start.y - 1].visited == false)
                        {
                            laby[(char)start.x, (char)start.y].value -= (char)WallEnum.Left;
                            laby[(char)start.x, (char)start.y - 1].value -= (char)WallEnum.Right;
                            GenLabyrinth(ref laby, new Vector2(start.x, start.y - 1), algo, width, height, ref fin);
                        }
                        break;
                }
                voisinDispo.RemoveAt(randCase);
            }
        }

        public static void createWalls(int width, int height, out tableau[,] laby)
        {
            laby = new tableau[height, width];
            int i, j;
            for (i = 0; i < height; i++)
            {
                for (j = 0; j < width; j++)
                {
                    laby[i, j].value |= (char)WallEnum.Top;
                    laby[i, j].value |= (char)WallEnum.Left;
                    laby[i, j].value |= (char)WallEnum.Bottom;
                    laby[i, j].value |= (char)WallEnum.Right;
                }
            }
        }


        /// <summary>
        /// Fonction permettant de d�finir si la position d'entr�e est sur un bord du labyrinthe
        /// </summary>
        /// <param name="position">
        /// Vecteur 2D indiquant la position de la case s�lectionn�e
        /// </param>
        /// <param name="width">
        /// Largeur du labyrinthe
        /// </param>
        /// <param name="height">
        /// Hauteur du labyrinthe
        /// </param>
        /// <returns>
        /// Renvoie en code binaire quels murs ne sont pas des bords du labyrinthe
        /// 
        /// HAUT = 0001
        /// DROITE = 0010
        /// BAS = 0100
        /// GAUCHE = 1000
        /// 
        /// 
        /// EX : case(0,0), on est en haut � gauche du labyrinthe il n'y a donx que deux murs n'�tant
        /// pas des bords, � droite et en bas. On renvoie alors le code 0110
        /// </returns>
        static byte CheckBorder(Vector2 position, int width, int height)
        {
            byte border = 0;
            if (position.x != 0)
            {
                border |= (byte)WallEnum.Top;
            }
            if (position.x != height-1)
            {
                border |= (byte)WallEnum.Bottom;
            }
            if (position.y != 0)
            {
                border |= (byte)WallEnum.Left;
            }
            if (position.y != width-1)
            {
                border |= (byte)WallEnum.Right;
            }
            return border;
        }

        /// <summary>
        /// Renvoie les voisins qui n'ont pas �t� visit� autour de la case s�lectionn�e
        /// </summary>
        /// <param name="laby">
        /// Tableau contenant toutes les cases du labyrinthe
        /// </param>
        /// <param name="border">
        /// Code binaire retourn� par la fonction CheckBorder
        /// </param>
        /// <returns>
        /// Code binaire indiquant les voisins qui n'ont pas �t� visit�
        /// 
        /// HAUT = 0001
        /// DROITE = 0010
        /// BAS = 0100
        /// GAUCHE = 1000
        /// 
        /// EX : border = 0011, on regarde si le voisin du haut et celui de droite ont �t� visit�.
        /// seulement le voisin de droite n'a pas �t� visit�, on renvoie 0010
        /// </returns>
        static byte Visited(ref tableau[,] laby, Vector2 position, int width, int height, byte border)
        {
            byte notvisited = 0;
            if ((border & 1) != 0)
            {
                if (!(laby[(int)position.x - 1, (int)position.y].visited))
                {
                    notvisited |= 1;
                }
            }
            if ((border & 2) != 0)
            {
                if (!(laby[(int)position.x, (int)position.y + 1].visited))
                {
                    notvisited |= 2;
                }
            }
            if ((border & 4) != 0)
            {
                if (!(laby[(int)position.x + 1, (int)position.y].visited))
                {
                    notvisited |= 4;
                }
            }
            if ((border & 8) != 0)
            {
                if (!(laby[(int)position.x, (int)position.y - 1].visited))
                {
                    notvisited |= 8;
                }
            }
            return notvisited;
        }
        */

    }


}