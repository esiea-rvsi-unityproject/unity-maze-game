using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateKeyOnStart : MonoBehaviour
{
    [SerializeField]
    private GameItemID keyID;

    [SerializeField]
    private int layer;

    [SerializeField]
    private Camera renderingCamera;

    private bool rendered = false;

    private void Update()
    {
        if (rendered) return;

        if (keyID != GameItemID.RedSectorKey && keyID != GameItemID.BlueSectorKey && keyID != GameItemID.GreenSectorKey) return;

        KeyFactory factory = GameObject.FindObjectOfType<KeyFactory>();

        GameObject key = factory.GenerateSectorKey(keyID);
        key.transform.position = Vector3.zero;
        key.layer = layer;
        key.transform.SetParent(transform);
        key.name = "Render key";

        renderingCamera.Render();

        rendered = true;
    }
}
