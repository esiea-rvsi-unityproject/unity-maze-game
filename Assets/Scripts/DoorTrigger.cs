using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    public DoorListener doorListener;

    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("Player"))
            doorListener.OpenDoor(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name.Contains("Player"))
            doorListener.CloseDoor();
    }
}
