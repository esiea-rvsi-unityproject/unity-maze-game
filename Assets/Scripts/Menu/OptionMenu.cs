using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionMenu : MonoBehaviour
{
    public AudioMixer audioMixer;

    public TMPro.TMP_Dropdown drop_res;

    Resolution[] resolutions;

    void Start()
    {
        List<Resolution> tmpres = new List<Resolution>();
        foreach (var res in Screen.resolutions)
        {
            if(res.refreshRate == Screen.currentResolution.refreshRate)
            {
                tmpres.Add(res);
            }
        }
        resolutions = tmpres.ToArray();

        drop_res.ClearOptions();

        List<string> resolutions_string = new List<string>();

        int currResolution = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height + " - " +resolutions[i].refreshRate + " Hz";
            resolutions_string.Add(option);

            if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currResolution = i;
            }
        }

        drop_res.AddOptions(resolutions_string);
        drop_res.value = currResolution;
        drop_res.RefreshShownValue();
    }
    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("Volume", volume);
    }

    public void SetFullscreen(bool fs)
    {
        Screen.fullScreen = fs;
    }

    public void SetResolution(int index)
    {
        Resolution newRes = resolutions[index];
        Screen.SetResolution(newRes.width,newRes.height, Screen.fullScreen);
    }
}
