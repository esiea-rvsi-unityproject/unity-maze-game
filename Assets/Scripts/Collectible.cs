using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public GameItemID Item { get => itemID; set => itemID = value; }

    [SerializeField]
    private GameItemID itemID = GameItemID.None;


    private void OnTriggerEnter(Collider other)
    {
        Inventory inventory = other.GetComponent<Inventory>();
        if (inventory != null)
        {
            inventory.Add(itemID);
            Destroy(gameObject);
        }
    }
}
