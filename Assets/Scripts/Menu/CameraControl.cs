using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public GameObject ground;

    private Collider g_collider;
    private Vector3 center;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(10, 12, -12);
        g_collider = ground.GetComponent<Collider>();
        gameObject.transform.LookAt(g_collider.bounds.center);
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(g_collider.bounds.center, Vector3.up, 20 * Time.deltaTime);
    }
}
