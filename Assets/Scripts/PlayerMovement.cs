using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    public BetterCamera Camera;
    public float PlayerSpeed = 5.0f;

    [SerializeField]
    private Animator animator;

    private CharacterController _controller = null;

    [SerializeField]
    private NavMeshAgent _agent;

    private float turnSmoothVelocity;
    private float TurnSmoothTime = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<CharacterController>();
        if (_controller == null) Debug.LogWarning("No character controller attached to player");
        _agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 axisInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        bool isRunning = false;
        if (!_agent.enabled) {
            if (axisInput.magnitude > 0.1f)
            {
                Vector3 targetForward, targetRight;
                Camera.GetTargetDirections(out targetForward, out targetRight);
                Vector3 playerMovement = (targetRight * axisInput.x + targetForward * axisInput.y).normalized;
                _controller.Move(playerMovement * Time.deltaTime * PlayerSpeed);

                Vector3 direction = new Vector3(axisInput.x, 0f, axisInput.y).normalized;
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + Camera.CurrentAngles.x;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, TurnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                isRunning = true;
            }
        } else
        {
            isRunning = _agent.desiredVelocity != Vector3.zero;
        }

        animator.SetBool("isRunning", isRunning);
    }
}
