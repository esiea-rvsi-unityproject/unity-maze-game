using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterCamera : MonoBehaviour
{
    struct CameraState
    {
        public Vector2 angles;
        public float zoom;
        public Vector3 position;
    }

    public Vector2 CameraSensivity = Vector2.one * 50.0f;
    public float CameraZoomSensivity = 50f;

    public Vector3 CameraOffset = Vector3.zero;

    public float InitialCameraZoom = 5f;
    public Vector2 CameraZoomInterval = new Vector2(2f, 10f);

    public float CameraAnglesLerpCoef = .5f;
    public float CameraToPlayerLerpCoef = .5f;
    public float CameraZoomLerpCoef = .5f;

    public bool Locked { get => locked; }

    private CameraState currentState;
    private CameraState targetState;
    private CameraState stateBeforeLock;

    [SerializeField]
    private GameObject _cameraRotator1;
    [SerializeField]
    private GameObject _cameraRotator2;
    [SerializeField]
    private GameObject _camera;

    private bool locked = false;

    public Vector2 CurrentAngles { get => currentState.angles; }

    public void GoToPosition(Vector3 pos)
    {
        targetState.position = pos;
    }

    public void GetTargetDirections(out Vector3 forward, out Vector3 right)
    {
        forward = Vector3.zero;
        forward.x = Mathf.Sin(targetState.angles.x * Mathf.Deg2Rad);
        forward.z = Mathf.Cos(targetState.angles.x * Mathf.Deg2Rad);

        right = Vector3.Cross(Vector3.up, forward);
    }
    public void GetCurrentDirections(out Vector3 forward, out Vector3 right)
    {
        forward = Vector3.zero;
        forward.x = Mathf.Sin(targetState.angles.x * Mathf.Deg2Rad);
        forward.z = Mathf.Cos(targetState.angles.x * Mathf.Deg2Rad);

        right = Vector3.Cross(Vector3.up, forward);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (_cameraRotator1 == null) Debug.LogWarning("[BetterCamera] Camera Rotator 1 is missing !");
        if (_cameraRotator2 == null) Debug.LogWarning("[BetterCamera] Camera Rotator 2 is missing !");
        if (_camera == null) Debug.LogWarning("[BetterCamera] Camera is missing !");

        InitialCameraZoom = Mathf.Clamp(InitialCameraZoom, CameraZoomInterval.x, CameraZoomInterval.y);

        currentState = new CameraState
        {
            angles = Vector2.zero,
            zoom = InitialCameraZoom,
            position = transform.position
        };

        targetState = new CameraState
        {
            angles = Vector2.zero,
            zoom = InitialCameraZoom,
            position = transform.position
        };

        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y"));
        float mouseWheel = Input.GetAxis("Mouse ScrollWheel");
        if (locked)
        {
            mouseInput = Vector2.zero;
            mouseWheel = 0f;
        }

        // Rotate camera according to input
        targetState.angles += mouseInput * CameraSensivity * Time.deltaTime;
        targetState.angles.y = Mathf.Clamp(targetState.angles.y, -90f, 90f);

        // Zoom camera according to input
        targetState.zoom -= mouseWheel * CameraZoomSensivity * Time.deltaTime;
        targetState.zoom = Mathf.Clamp(targetState.zoom, CameraZoomInterval.x, CameraZoomInterval.y);


        float targetCameraZoom = targetState.zoom;

        // Camera callision with scene objects
        RaycastHit hit;
        Ray cameraCollisionRay = new Ray
        {
            origin = transform.position,
            direction = (_camera.transform.position - transform.position).normalized
        };
        if (Physics.Raycast(cameraCollisionRay, out hit, CameraZoomInterval.y))
            targetCameraZoom = Mathf.Clamp(targetState.zoom, CameraZoomInterval.x, hit.distance * 0.9f);

        // Lerp transform
        currentState.position = LerpVector3(currentState.position, targetState.position, CameraToPlayerLerpCoef);
        currentState.angles = LerpAngleVector2(currentState.angles, targetState.angles, CameraAnglesLerpCoef);
        currentState.zoom = Mathf.Lerp(currentState.zoom, targetCameraZoom, CameraZoomLerpCoef);

        SetPosition(currentState.position);
        SetAngles(currentState.angles);
        SetCameraZoom(currentState.zoom);
    }

    public void LockFollowingRay(Ray ray)
    {
        stateBeforeLock = targetState;
        locked = true;
        targetState.position = ray.origin;
        targetState.angles = Quaternion.LookRotation(ray.direction).eulerAngles;
    }

    public void UnlockCamera()
    {
        targetState = stateBeforeLock;
        locked = false;
    }

    private void SetPosition(Vector3 pos)
    {
        transform.position = pos;
    }

    private void SetAngles(Vector2 angles)
    {
        _cameraRotator1.transform.localRotation = Quaternion.Euler(Vector3.up * angles.x);
        _cameraRotator2.transform.localRotation = Quaternion.Euler(Vector3.right * angles.y);
    }

    private void SetCameraZoom(float zoom)
    {
        _camera.transform.localPosition = -zoom * Vector3.forward;
    }

    private Vector2 LerpAngleVector2(Vector2 original, Vector2 target, float t)
    {
        return new Vector2(
            Mathf.LerpAngle(original.x, target.x, t),
            Mathf.LerpAngle(original.y, target.y, t)
            );
    }

    private Vector3 LerpVector3(Vector3 original, Vector3 target, float t)
    {
        return new Vector3(
            Mathf.Lerp(original.x, target.x, t),
            Mathf.Lerp(original.y, target.y, t),
            Mathf.Lerp(original.z, target.z, t)
            );
    }
}
