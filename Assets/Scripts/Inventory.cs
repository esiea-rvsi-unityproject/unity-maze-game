using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum GameItemID
{
    RedSectorKey,
    BlueSectorKey,
    GreenSectorKey,
    MainKey,
    NbIds,
    None
}

public class Inventory : MonoBehaviour
{
    public class ItemStack
    {
        public GameItemID item;
        public int count;
    }
    
    public int NbDistinctItems { get => items.Count; }

    List<ItemStack> items;

    public UnityAction<GameItemID,int> GameItemAdded;

    public UnityAction<GameItemID> GameItemRemoved;

    // Start is called before the first frame update
    void Start()
    {
        items = new List<ItemStack>();
    }

    public void Add(GameItemID item)
    {
        if (item == GameItemID.None) return;
        int index = FindItem(item);
        if (index >= 0)
            items[index].count++;
        else {
            index = items.Count;
            items.Add(new ItemStack
            {
                item = item,
                count = 1
            });
        }
        GameItemAdded(item, items[index].count);
    }

    public void RemoveAll(GameItemID item)
    {
        int index = FindItem(item);
        if (index >= 0)
            items.RemoveAt(index);
        GameItemRemoved(item);
    }

    public void Remove(GameItemID item, int count = 1)
    {
        int index = FindItem(item);
        if (index >= 0)
        {
            items[index].count -= count;
            if (items[index].count == 0)
                items.RemoveAt(index);
            GameItemRemoved(item);
        }
    }

    public GameItemID Get(int index)
    {
        if (index < 0 || index >= items.Count) return GameItemID.None;
        return items[index].item;
    }

    public int Nb(GameItemID item)
    {
        int index = FindItem(item);
        if (index >= 0)
            return items[index].count;
        return 0;
    }

    private int FindItem(GameItemID item)
    {
        return items.FindIndex(itemStack => itemStack.item == item);
    }
}