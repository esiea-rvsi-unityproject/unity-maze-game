using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using MazeGeneration;

public enum AlgoEnum
{
    Rbacktracker,
    Eller,
    Kruskal,
    Prim,
    AldousBroder,
    Wilson,
    GrowTree
}

public struct tableau
{
    public int value;
    public bool visited;
}

public class LevelManager : MonoBehaviour
{
    //On d�fini les variables pour le nombre de case en hauteur et en largeur du labyrinthe
    [SerializeField]
    public int width, height;

    [SerializeField]
    Terrain sol;

    [SerializeField]
    GameObject wallPrefab;

    [SerializeField]
    GameObject doorPrefab;

    [SerializeField]
    Material wallMaterial;

    [SerializeField]
    Material doorMaterial;

    [SerializeField]
    Color[] sectorColors;

    [SerializeField]
    GameObject finalAreaPrefab;

    [SerializeField]
    GameObject treePrefab;

    [SerializeField]
    GameObject parent;

    [SerializeField]
    GameObject playerPrefab;

    GameObject player;

    [SerializeField]
    NavMeshSurface navigation;

    [SerializeField]
    Transform target;

    [SerializeField]
    GameObject camera;

    [SerializeField]
    GameObject sectorKeyPrefab;

    [SerializeField]
    GameObject mainKeyPrefab;

    public Vector2 fin;

    private Labyrinth labyrinth;

    NavMeshAgent agent;

    // Start is called before the first frame update
    private void Start()
    {
        GenMap();
    }

    // Update is called once per frame
    private void Update()
    {
        // if (agent.enabled)
        //     agent.destination = target.position;
        // if ((player.transform.position.x > fin.x && player.transform.position.x < fin.x + 1) && (player.transform.position.z > fin.y && player.transform.position.z < fin.y + 1))
        // {
        //     Destroy(GameObject.FindWithTag("Laby"));
        //     SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        // }
    }

    private void GenMap()
    {
        //Cr�ation des variables : laby contient toutes les cases du labyrinthe, x et y sont les variables de d�parts pour l'algorithme de cr�ation du labyrinthe 
        Vector2Int from = Vector2Int.zero;
        if (Random.Range(0f, 1f) < 0.5f)
        {
            from.x = Random.Range(1, width - 1);
            from.y = Random.Range(0, 2) * (height - 1);
        } else
        {
            from.x = Random.Range(0, 2) * (width - 1);
            from.y = Random.Range(1, height - 1);
        }

        Debug.Log($"From: {from}");

        //Instanciation du labyrinthe
        labyrinth = MazeGenerator.GenerateLabyrinth(width, height, from);
        MazeGenerator.GenerateTrees(ref labyrinth, Random.Range(1, Mathf.CeilToInt(Mathf.Sqrt(width * height) / 3f)));
        MazeGenerator.RemoveBorderWallNextToPosition(ref labyrinth, from);
        labyrinth.tiles[from.x, from.y].content.Clear();

        CreateLabyrinthInScene(ref labyrinth);

        // Cr�ation de la zone de fin
        var finalArea = Instantiate(finalAreaPrefab);
        if (from.x == 0)
        {
            finalArea.transform.position = new Vector3(from.x, 0, from.y + 0.5f);
        }
        else if (from.x == width - 1)
        {
            finalArea.transform.position = new Vector3(from.x+1, 0, from.y + 0.5f);
            finalArea.transform.rotation = Quaternion.Euler(Vector3.up * 180f);
        }
        else if (from.y == 0)
        {
            finalArea.transform.position = new Vector3(from.x + 0.5f, 0, from.y);
            finalArea.transform.rotation = Quaternion.Euler(-Vector3.up * 90f);

        } else if (from.y == height - 1)
        {
            finalArea.transform.position = new Vector3(from.x + 0.5f, 0, from.y + 1);
            finalArea.transform.rotation = Quaternion.Euler(Vector3.up * 90f);
        }

        //Cr�ation du mesh de navigation
        navigation.BuildNavMesh();


        // Cr�ation du jouer
        player = Instantiate(playerPrefab);

        player.transform.position = new Vector3(from.x + 0.5f, 0.5f, from.y + 0.5f);

        camera.GetComponent<BetterCameraManipulator>().Player = player.transform;
        player.GetComponent<PlayerMovement>().Camera = camera.GetComponent<BetterCamera>();
     
        
        // Cible du path finding
        target.transform.position = new Vector3(fin.x+0.5f, 1, fin.y+0.5f);
    }

    private GameObject InstantiateSectorKey(TileContentType keyType)
    {
        if (keyType != TileContentType.RedKey && keyType != TileContentType.BlueKey && keyType != TileContentType.GreenKey)
            return null;

        GameItemID id = GameItemID.NbIds;

        switch (keyType) {
            case TileContentType.RedKey:
                id = GameItemID.RedSectorKey;
                break;
            case TileContentType.BlueKey:
                id = GameItemID.BlueSectorKey;
                break;
            case TileContentType.GreenKey:
                id = GameItemID.GreenSectorKey;
                break;
        }

        GameObject res = KeyFactory.Instance.GenerateSectorKey(id);
        Collectible collectible = res.GetComponent<Collectible>();
        collectible.Item = id;

        return res;
    }

    private Color GetLineColorFromSector(LabyrinthSector sector)
    {
        int index = (int)sector - (int)LabyrinthSector.None;
        if (index < 0 || index >= sectorColors.Length) return Color.black;
        return sectorColors[index];
    }

    private void CreateLabyrinthInScene(ref Labyrinth labyrinth)
    {
        sol.transform.position = new Vector3(0, 0, 0);
        sol.terrainData.size = new Vector3(height, 1, width);

        System.Action<Labyrinth.Tile, TileDirection> CreateWall = (tile, direction) =>
        {
            Labyrinth.TileLink link = tile.links[(int)direction];

            if (link.type == TileLinkType.Empty) return;


            GameObject linkObject = null;
            Renderer renderer = null;

            switch (link.type)
            {
                case TileLinkType.Wall:
                    linkObject = Instantiate(wallPrefab);
                    renderer = linkObject.GetComponent<Renderer>();
                    renderer.material = wallMaterial;

                    LabyrinthSector tileSector = tile.sector;
                    LabyrinthSector neighbourSector = tile.GetNeighbourTile(direction)?.sector ?? LabyrinthSector.None;

                    Color lineColor1 = GetLineColorFromSector(tileSector);
                    Color lineColor2 = GetLineColorFromSector(neighbourSector);

                    renderer.material.SetColor("LineColor1", lineColor1);
                    renderer.material.SetColor("LineColor2", lineColor2);

                    if (tileSector == LabyrinthSector.None)
                        renderer.material.SetFloat("LineEmission1", 0f);
                    else renderer.material.SetFloat("LineEmission1", 1f);
                    if (neighbourSector == LabyrinthSector.None)
                        renderer.material.SetFloat("LineEmission2", 0f);
                    else renderer.material.SetFloat("LineEmission2", 1f);

                    break;

                case TileLinkType.Door:
                    linkObject = Instantiate(doorPrefab);
                    renderer = linkObject.GetComponent<Renderer>();
                    renderer.material = doorMaterial;
                    Labyrinth.DoorLink doorLink = link as Labyrinth.DoorLink;
                    renderer.material.SetColor("LineColor", GetLineColorFromSector(doorLink.sector));
                    break;
            }

            linkObject.transform.SetParent(parent.transform);

            switch (direction)
            {
                case TileDirection.Top:
                    linkObject.transform.localPosition = new Vector3(tile.x + 0.5f, 0.5f, tile.y);
                    linkObject.transform.RotateAround(linkObject.transform.position, new Vector3(0, 1, 0), 180);
                    break;

                case TileDirection.Bottom:
                    linkObject.transform.localPosition = new Vector3(tile.x + 0.5f, 0.5f, tile.y + 1);
                    break;

                case TileDirection.Left:
                    linkObject.transform.localPosition = new Vector3(tile.x, 0.5f, tile.y + 0.5f);
                    linkObject.transform.RotateAround(linkObject.transform.position, new Vector3(0, 1, 0), -90);
                    break;

                case TileDirection.Right:
                    linkObject.transform.localPosition = new Vector3(tile.x + 1, 0.5f, tile.y + 0.5f);
                    linkObject.transform.RotateAround(linkObject.transform.position, new Vector3(0, 1, 0), 90);
                    break;
            }

            if (link.type == TileLinkType.Door)
            {
                linkObject.transform.localPosition = linkObject.transform.localPosition - Vector3.up * 0.25f;

                DoorListener listener = linkObject.AddComponent<DoorListener>();
                
                // G�n�re les colliders pour ouvrir la porte
                for (int i = 0; i < link.tiles.Length; i++)
                {
                    GameObject triggerObject = new GameObject("Door trigger");
                    triggerObject.transform.SetParent(linkObject.transform);
                    triggerObject.transform.position = new Vector3(link.tiles[i].x + 0.5f, 0f, link.tiles[i].y + 0.5f);
                    BoxCollider collider = triggerObject.AddComponent<BoxCollider>();
                    collider.center = collider.center + 0.5f * Vector3.up;
                    collider.isTrigger = true;
                    DoorTrigger doorTrigger = triggerObject.AddComponent<DoorTrigger>();
                    doorTrigger.doorListener = listener;
                }

                Labyrinth.DoorLink doorLink = link as Labyrinth.DoorLink;
                switch (doorLink.sector)
                {
                    case LabyrinthSector.Red:
                        listener.keyID = GameItemID.RedSectorKey;
                        break;
                    case LabyrinthSector.Green:
                        listener.keyID = GameItemID.GreenSectorKey;
                        break;
                    case LabyrinthSector.Blue:
                        listener.keyID = GameItemID.BlueSectorKey;
                        break;
                }
            }
        };

        System.Action<Labyrinth.Tile> CreateCollectible = (tile) =>
        {
            bool hasRedKey = tile.content.Contains(TileContentType.RedKey);
            bool hasBlueKey = tile.content.Contains(TileContentType.BlueKey);
            bool hasGreenKey = tile.content.Contains(TileContentType.GreenKey);
            bool hasSectorKey = hasRedKey || hasBlueKey || hasGreenKey;
            bool hasMainKey = tile.content.Contains(TileContentType.MainKey);
            if (!hasSectorKey && !hasMainKey) return;

            GameObject collectibleGameObject = null;
            if (tile.content.Count > 1)
            {
                collectibleGameObject = new GameObject("Key holder");
                collectibleGameObject.AddComponent<CollectibleHolder>();

                for (int i = 0; i < tile.content.Count; i++)
                {
                    GameObject keyObject = null;
                    if (tile.content[i] == TileContentType.MainKey)
                        keyObject = Instantiate(mainKeyPrefab);
                    else if (tile.content[i] != TileContentType.Empty)
                        keyObject = InstantiateSectorKey(tile.content[i]);
                    keyObject.transform.SetParent(collectibleGameObject.transform);
                }
            }
            else
            {
                if (tile.content[0] == TileContentType.MainKey)
                    collectibleGameObject = Instantiate(mainKeyPrefab);
                else if (tile.content[0] != TileContentType.Empty)
                    collectibleGameObject = InstantiateSectorKey(tile.content[0]);
            }
            collectibleGameObject.transform.position = new Vector3(tile.x + 0.5f, 0.5f, tile.y + 0.5f);
        };

        System.Action<Labyrinth.Tile> CreateTrees = (tile) =>
        {
            if (!tile.content.Contains(TileContentType.Tree)) return;

            GameObject tree = Instantiate(treePrefab);

            tree.transform.position = new Vector3(tile.x + 0.5f, 0f, tile.y + 0.5f);
            tree.transform.rotation = Quaternion.Euler(Vector3.up * Random.Range(0, 360f));
        };

        for (int i = 0; i < labyrinth.height; i++)
            CreateWall(labyrinth.tiles[0, i], TileDirection.Left);
        for (int i = 0; i < labyrinth.width; i++)
            CreateWall(labyrinth.tiles[i, 0], TileDirection.Top);

        for (int i = 0; i < labyrinth.width; i++)
            for (int j = 0; j < labyrinth.height; j++) {
                CreateWall(labyrinth.tiles[i, j], TileDirection.Bottom);
                CreateWall(labyrinth.tiles[i, j], TileDirection.Right);
                CreateCollectible(labyrinth.tiles[i, j]);
                CreateTrees(labyrinth.tiles[i, j]);
            }

    }

    /*
    private void GenMap()
    {
        //Cr�ation des variables : laby contient toutes les cases du labyrinthe, x et y sont les variables de d�parts pour l'algorithme de cr�ation du labyrinthe 
        tableau[,] laby;
        int x = Random.Range(0, height);
        int y = Random.Range(0, width);

        //Instanciation du labyrinthe
        //M�thode permettant de cr�er un labyrinthe avec � chaque case 4 murs
        MazeGenerator.createWalls(width, height, out laby);
        //M�thode supprimant certains murs de cases afin de cr�er le labyrinthe
        MazeGenerator.GenLabyrinth(ref laby, new Vector2(x, y), AlgoEnum.Rbacktracker, width, height, ref fin);
        Debug.Log("Position de d�but : " + x + " " + y);
        Debug.Log("Position de fin : " + fin.x + " " + fin.y);
        GenWalls(in laby, width, height);
        //On change la taille et la position du sol en fonction de la taille du labyrinthe
        sol.transform.position = new Vector3(0, 0, 0);
        sol.terrainData.size = new Vector3(height, 1, width);

        //Cr�ation du mesh de navigation
        navigation.BuildNavMesh();

        player = Instantiate(playerPrefab);
        //On d�place le joueur sur la case de d�part de l'algo
        player.transform.position = new Vector3(x + 0.5f, 0.5f, y);

        camera.GetComponent<BetterCameraManipulator>().Player = player.transform;
        player.GetComponent<PlayerMovement>().Camera = camera.GetComponent<BetterCamera>();
        agent = player.GetComponent<NavMeshAgent>();
        if (agent == null)
            agent = player.AddComponent<NavMeshAgent>();

        target.transform.position = new Vector3(fin.x + 0.5f, 1, fin.y + 0.5f);
        Debug.Log(agent.path.corners.Length);
    }

    void GenWalls(in tableau[,] laby, int width, int height)
    {
        int i, j;
        for (i = 0; i < height; i++)
        {
            for (j = 0; j < width; j++)
            {
                if ((laby[i, j].value & (char)WallEnum.Top) != 0)
                {
                    GameObject mur = Instantiate(murs);
                    mur.transform.position = new Vector3(i, 0.5f, j);
                    mur.transform.SetParent(parent.transform);
                    mur.transform.RotateAround(mur.transform.position, new Vector3(0, 1, 0), 90);
                    mur.GetComponent<Renderer>().material.color = Color.red;
                }

                if ((laby[i, j].value & (char)WallEnum.Bottom) != 0)
                {
                    GameObject mur = Instantiate(murs);
                    mur.transform.position = new Vector3(i + 1, 0.5f, j);
                    mur.transform.SetParent(parent.transform);
                    mur.transform.RotateAround(mur.transform.position, new Vector3(0, 1, 0), 90);
                    mur.GetComponent<Renderer>().material.color = Color.blue;
                }
                if ((laby[i, j].value & (char)WallEnum.Left) != 0)
                {
                    GameObject mur = Instantiate(murs);
                    mur.transform.SetParent(parent.transform);
                    mur.transform.position = new Vector3(i + 0.5f, 0.5f, j - 0.5f);
                    mur.GetComponent<Renderer>().material.color = Color.green;
                }

                if ((laby[i, j].value & (char)WallEnum.Right) != 0)
                {
                    GameObject mur = Instantiate(murs);
                    mur.transform.SetParent(parent.transform);
                    mur.transform.position = new Vector3(i + 0.5f, 0.5f, j + 0.5f);
                    mur.GetComponent<Renderer>().material.color = Color.yellow;
                }
            }
        }
    }*/
}