using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionScript : MonoBehaviour
{
    public Animator transition;

    public float TimeTransition = 1f;

    public void LoadGame()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }

    public void LoadMenu()
    {
        Cursor.visible = true;
        Time.timeScale = 1f;
        StartCoroutine(LoadLevel(0));
    }

    IEnumerator LoadLevel(int index)
    {
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(TimeTransition);

        SceneManager.LoadScene(index);
    }
}
