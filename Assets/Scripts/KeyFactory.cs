using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MazeGeneration;

public class KeyFactory : MonoBehaviour
{
    public static KeyFactory Instance { get; private set; }

    [SerializeField]
    private GameObject sectoryKeyPrefab;

    [SerializeField]
    private Color RedKeyColor;

    [SerializeField]
    private Color GreenKeyColor;

    [SerializeField]
    private Color BlueKeyColor;

    private void Awake()
    {
        Instance = this;
    }

    public GameObject GenerateSectorKey(GameItemID id)
    {
        GameObject key = Instantiate(sectoryKeyPrefab);
        Color color = Color.black;
        switch (id)
        {
            case GameItemID.RedSectorKey:
                color = RedKeyColor;
                break;
            case GameItemID.GreenSectorKey:
                color = GreenKeyColor;
                break;
            case GameItemID.BlueSectorKey:
                color = BlueKeyColor;
                break;
        }
        key.GetComponent<Renderer>().material.color = color;
        return key;
    }
}
