using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorListener : MonoBehaviour
{
    private const string IS_DOOR_OPENED = "IsDoorOpened";

    public GameItemID keyID;

    private bool unlocked = false;
    private int activeTriggers;

    private Animator animator;
    private BoxCollider collider;

    private void Start()
    {
        activeTriggers = 0;
        animator = GetComponent<Animator>();
        collider = GetComponent<BoxCollider>();
    }

    private void UpdateDoor()
    {
        if (activeTriggers > 0 && unlocked)
        {
            animator.SetBool(IS_DOOR_OPENED, true);
            collider.enabled = false;
        }
        else
        {
            animator.SetBool(IS_DOOR_OPENED, false);
            collider.enabled = true;
        }
    }

    public void OpenDoor(GameObject player)
    {
        if (!unlocked)
        {
            Inventory inventory = player.GetComponent<Inventory>();
            if (inventory == null) return;
            for (int i = 0; i < inventory.NbDistinctItems; i++)
            {
                GameItemID item = inventory.Get(i);
                if (item == keyID)
                {
                    inventory.RemoveAll(item);
                    unlocked = true;
                    break;
                }
            }
        }

        activeTriggers++;
        UpdateDoor();
    }

    public void CloseDoor()
    {
        activeTriggers--;
        UpdateDoor();
    }
}
